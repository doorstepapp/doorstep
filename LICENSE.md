# Doorstep License

This file is a quick reference of the Doorstep License. For full terms, please see your **Doorstep License Agreement** which you received at the time of purchase.

## Versioning
---

Different "versions" of Doorstep are referenced by the numbers that follow the software title [e.g. Doorstep 2.1]. The number that precedes the decimal point refers to a "major release". Changing to the latest major release is described as an "upgrade" and the new package will contain major enhancements and new functionality.

The number [or numbers] that follows the decimal point refers to a "minor release" and moving to the latest minor release is an "update" [e.g. Doorstep 2.1]. The new package will contain minor enhancements and fixes to the immediately previous version of the software referenced by the version number.

Under this Licence Agreement, you are entitled to download, install and use updates to the version for which you have purchased a licence. However, this Licence Agreement does not entitle you to upgrades. If you wish to upgrade to the latest major release, you will need to purchase the upgrade licence at cost.

After new major releases we may at our discretion, for a limited amount of time, continue to support and update both the current major release and the previous major release. Ultimately however, we will discontinue updates and, eventually, support for earlier major releases. Although not mandatory, for security reasons we encourage you to move to the current version wherever possible as soon as you feel comfortable doing so.

New Versions may contain new functionality and features which have been planned and implemented. We may also remove old functionality or features that are no longer relevant. You should read the advice relating to a new Version to make sure that you are happy and comfortable it contains all the functionality and features you require before installing an Update or an Upgrade. We will not be liable under and circumstances for any loss or inconvenience you suffer as a result of a change in the specification of Doorstep from one Version to another.

## License Grant
---

When you purchase Doorstep, you will pay the Licence Fee to us. In return, we hereby grant you a right to download, install and use Doorstep in accordance with the terms of this Licence Agreement. You acknowledge that this licence is non-exclusive, which means we may grant licences to other people, some of whom may be your direct competitors.
In addition to using Doorstep, you may also make a copy of it in the form in which you download it from us but you may only use that copy as a backup. You must save a copy of this licence alongside that back- up copy and you must keep the back-up copy somewhere secure. If we ask, you must tell us where the back-up copy is stored and, if appropriate, give us access to it so that we can check and confirm.

## License Limitations
---
When you purchase Doorstep, we sell to you our permission to use it. However, that permission is not limitless. It is subject to a number of limitations.

Without our explicit written consent, you may not:
* share, transfer or distribute copies of Doorstep or Documentation to others; nor
* transmit, publish, display, reproduce, edit, translate, modify, frame, create derivative works or otherwise alter Doorstep; nor
* reverse engineer, decompile or disassemble Doorstep save as permitted by law; nor
* delete, vary or obscure our copyright notices or other proprietary notices.

You may use each Doorstep License for one Project only. However, in addition to the domain upon which you will eventually use the Project [e.g. example.com], you may also nominate a further two domains for that project, one as a development site and one as a staging version [e.g. stage.example.com and example.local]. If we suspect that you are using one licensed copy of Doorstep for two or more projects, we will disable those domains until you purchase the appropriate number of licences and if you do it repeatedly, we may terminate all of your Doorstep licences.

Doorstep is designed and built to allow developers and designers to build websites for their clients, with the intention that the resulting projects should be easy to manage and administer. Although you are entitled to include Doorstep as part of the final product that you are delivering to your client for the purpose of using and administering the website that is the subject of the Project, you may not sub-license Doorstep repeatedly, whether as a hosted content or property management system or in any other way.

We do not grant you permission to distribute Doorstep, whether you do so intentionally or accidentally, for example by storing the Project in any form of open source control service. If you do use a source control service, you must ensure that it is secured for the use of you and your employees only. Doorstep must NOT be made available to the public under any circumstances and your failure to secure Doorstep when using a source control service will constitute an infringement of our Intellectual Property Rights.
Third Party Software packaged with Doorstep is included subject to the licence terms from the owners/controllers of the Intellectual Property Rights in the software in question, copies of which are provided on request and with Doorstep.

## Sub-licensing and assignments
---
Doorstep is intended to be an easy to use website property management tool as well as providing a maintainable foundation. Your Project's website may be intended for use by your own business [or that of your employer] or you may be building websites for clients. In the latter case, when you've finished building the website, you must decide whether to keep control of it or whether you to hand over control to your client.
If you are maintaining control of a client website, you are entitled under this Licence Agreement to grant a sub-licence of the Doorstep licence relating to the Project to your client. If you choose to do this, you will remain responsible to us for the compliance of your client [and its users and customers] with the terms of this Licence Agreement. If your client breaches any of the terms of this Licence Agreement, you will be liable to us for that breach. On the other hand, you will maintain control of the Project.

Alternatively, if you intend on handing the Project over to your client on completion and then exiting the arrangement, you may prefer to transfer the benefit of the Doorstep licence relating to the Project to your client. You can request this via writing and we will transfer the Doorstep license to your client, a representative of whom will have to confirm that they accept the terms of this Licence Agreement [other than, of course, the obligation to pay the Licence Fee, assuming that it has been paid]. From that point onwards, your client will be responsible for any breaches of this Licence Agreement, however you will no longer control the Project.

## Termination
---
Your right to download, install and use Doorstep in accordance with this Licence Agreement starts on the date we received the Licence Fee from you and continues indefinitely unless terminated in accordance with this clause.

We may terminate your Doorstep Licence if you breach or fail to comply with any of your obligations under this Agreement. If your breach or non- compliance can be fixed, we will contact you citing the breach or non-compliance and we will give you 14 days to correct the position. If you fail to do so, we may terminate your Doorstep Licence without further notice.
We may suspend your use of Doorstep rather than terminate this Agreement, thereby giving you an opportunity to put things right. Suspension as opposed to termination is entirely at our discretion.
We may also terminate your Licence:

* if you, as an individual, are declared bankrupt, become mentally incapacitated or die; or
* if you, being a company, go into liquidation, whether compulsory or voluntary [except for the purposes of any amalgamation or reconstruction] or have a receiver appointed over any of your assets or undertakings, or you are unable to pay your debts [within the meaning of section 123 of the Insolvency Act 1986] or you stop payment of any sums due under this Licence Agreement or you cease to carry on the whole or substantially the whole of your business;
and you must notify us immediately should any of these circumstances arise. Please note however, this will not affect any Licences that you have already transferred to your clients, although to be clear, were any of these events to happen to a client to whom you have already transferred a Doorstep licence, our right to terminate will still arise nonetheless.

Were you able to continue using Doorstep after we had terminated or suspended your Licence, you would be doing so without our permission. This would be an infringement of our Intellectual Property Rights. Consequently, we may build into Doorstep technological protection measures designed to ensure that your copy of Doorstep will be unusable once your Licence has been terminated or suspended.

We may, having terminated a Licence, require you to uninstall and remove Doorstep from any and all domains we have cited.

Should circumstances arise that give us cause to terminate a Licence, we may in addition terminate all Licences that you hold.

Please note that we do not reserve the right to terminate your Licence in the event that we cease trading or enter insolvency proceedings of any kind. Your rights under this Licence Agreement will survive any such proceedings and any purchaser of the Intellectual Property Rights in Doorstep will acquire those rights subject to your Licence.


## Support
---

We do not provide any sales advice or after-sales advice or service. Any support we give is gratuitous and provided entirely at our discretion. Due to other work commitments, we cannot provide support face- to-face or via telephone under any circumstances - our sole communication method is via email or via our Website.

Please note that any support we provide relates to the configuration and use of Doorstep. We do not provide support for the configuration of web servers or similar systems.

Our support may be limited to the most recent version of Doorstep. We may, at our discretion, cease to provide support for earlier versions of Doorstep at any time without notice.

You may have to update to the latest version available under your Licence and, failing to have done so already, we may ask you to complete the update before providing support.

Although we endevour to respond to support requests within a reasonable time, our ability to do so may be hindered by our capacity to take on new tasks at any given time. At present we do not offer support contracts or priority packages, nor do we currently have any plans to do so.

## Intellectual Property
---

Save for packaged Third Party Software, we own all Intellectual Property Rights in Doorstep and we warrant that your use of Doorstep in accordance with this Licence Agreement will not infringe the Intellectual Property Rights of any third party.

We will indemnify you against any loss, damage or expense that you suffer should we breach our warranty concerning the infringement of third party Intellectual Property Rights arising from the use of Doorstep providing that you notify us of any such actual or alleged infringement within 3 working days of your becoming aware or the same. This indemnity is also given on the basis that you will, should we so request, allow us to conduct all negotiations and litigation and that you will provide us with all reasonable assistance that we might request and that you will not attempt to compromise or settle any such infringement allegation.

We may, at our expense, modify or replace Doorstep in order to avoid the infringement of third party Intellectual Property Rights or we may terminate your Doorstep licence in order to prevent further infringements occurring. Should we terminate your Doorstep licence, we will refund the Licence Fee, subject to reasonable depreciation given the period through which you have been able to use Doorstep under the terms of this Licence Agreement.

You will respect our Intellectual Property Rights and those of the third parties who own or control the Third Party Software that is packaged with Doorstep. You gain no rights as a result of your purchase of your Doorstep licence or through your use of Doorstep, save as for the permission we grant you to use Doorstep under the terms of this Licence Agreement. You will indemnify us against all actions, claims, proceedings, damages, costs and expenses arising from your infringement of our Intellectual Property Rights in Doorstep.

Doorstep may contain technological protection measures designed to detect and/or prevent the infringement of our Intellectual Property Rights. You should be aware that circumventing such measures may constitute a criminal offence and we may, in the event that you attempt to circumvent those measures, terminate all Doorstep licences that you have purchased [including all licences that you have since assigned to third parties] immediately and without notice.

You will allow us to audit your use of Doorstep in order to ensure your compliance with this Licence Agreement, though any such audit will be carried out in good faith only where we have reasonable suspicion that such an infringement may have occurred.

You will notify us as soon as possible should you become aware that any third party is infringing our Intellectual Property Rights in Doorstep, whether that infringement arises from the unauthorised use, copying, distribution of Doorstep or any other act.

## Legal Stuff
---

While we always carry out functional testing throughout the development cycle, it is impossible to test for the absence of errors and we cannot guarantee that the functions contained in Doorstep, or related files, will always be entirely error-free. Doorstep is provided on an "as is" and an "as available" basis. We make no guarantee that Doorstep be entirely suitable for your intended use, neither do we guarantee that it will be error-free, timely, reliable, entirely secure, virus-free or available, especially since it is dependent on the reliability of the Internet and web server.

You use Doorstep entirely at your own risk. To the fullest extent permitted by law, Papertank shall not be liable to you or any third party for any costs, expenses, loss or damage [whether direct, indirect or consequential and whether economic or other] arising from the Client’s exercise of the rights granted to it under this License including but not limited to the use of Doorstep.

The rights and remedies conferred upon the Client and Papertank by this License Agreement are not [save as otherwise provided in this License Agreement] exclusive of any other rights or remedies otherwise provided by law.

No failure to exercise, and no delay in exercising, any right or remedy in connection with this License Agreement shall operate as a waiver of that right or remedy. No single or partial exercise of any right or remedy under this License Agreement shall preclude any other or further exercise of that right or remedy or the exercise of any other right or remedy. A waiver of any breach of this License Agreement shall not be deemed to be a waiver of any subsequent breach.

No waiver by either the Client or Doorstep of any of the requirements of this License Agreement or of any of its rights hereunder shall be effective unless given in writing and signed by or on behalf of that party and no forbearance, delay or indulgence by any party in enforcing the provisions of this License Agreement shall prejudice or restrict the rights of that party.

The provisions of this License Agreement shall be severable and distinct from one another and if one or more of these provisions is unenforceable, the remaining provisions shall continue unaffected. If any provision is held to be unenforceable that would be enforceable if part of its wording were deleted, the provision shall apply with such deletion as is necessary to make it enforceable.

The Client shall not assign, transfer, novate or otherwise dispose of any or all of its rights and obligations under this License Agreement without the prior written consent of Papertank Limited.

## Third-Party Software
---

### Framework
Doorstep utilises third-party commercial or open- source software developed by other programmers or companies, in particular the css interface framework Twitter Bootstrap as well as php framework Laravel. Papertank Limited are in no way responsible for the performance or quality of these third-party software products and we will not be held liable for any damages arising out of their use. As the obtainer of the finished website and, thus, the third-party software, you are bound by the third party software terms and license agreements which we can provide upon request.

### Mapping Provider

Doorstep integrates with mapping technology and services provided by Google Maps [Mapping Provider]. Papertank are not associated with the Mapping Provider and are not responsible for the support or reliability of their service / solution. Additionally, the Mapping Provider may set limits on the number of maps which can be displayed on your website. It is your responsibility to ensure your website visitors are within these limits.

### Property Portals

Doorstep provides the ability to export synchronous and real-time property feeds to select third party property portals including Rightmove [Portals]. Papertank are not associated with Portals and are not responsible for the support or reliability of their service / solution. Additionally, Doorstep is designed to work with the current version of their feed format, but Papertank cannot be liable for issues caused by changes from the Portals to their feed format.