<?php

namespace App\Providers;

use Doorstep\Feeds\FeedManager;
use Doorstep\Providers\FeedServiceProvider as ServiceProvider;

class FeedServiceProvider extends ServiceProvider
{
    /**
     * The feed driver mappings for the application.
     *
     * @var array
     */
    protected $drivers = [
        // 'rightmove-adf' => 'Doorstep\Feeds\Rightmove\RightmoveAdf',
    ];

    /**
     * Register any other drivers for your application.
     *
     * @param FeedManager $manager
     */
    public function boot(FeedManager $manager)
    {
        parent::boot($manager);

        //
    }
}
