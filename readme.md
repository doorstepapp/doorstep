# ![logo](http://doorstepapp.com/static/doc_logo.png)

## Property management web app

- Fully featured listings management for properties to rent
- Integration with property portals through automatic data feed.
- Integration with Google Maps and Streetview
- ‘Shortlist’ of properties can be added interactively on the front-end of the website, allowing the user to build up a list of potential properties.
- Automatic PDF generation allows the user to download an optimized for print view of the property details, photos and location.
- Powered by PHP and Laravel

## Requirements

- Ubuntu/Debian/CentOS
- Apache / Nginx
- PHP >= 5.4
- MCrypt, JSON, Fileinfo, GD PHP Extensions
- Composer - Dependency manager
- MySQL

## Authors

Doorstep is developed and maintained by [Papertank Limited](http://papertank.co.uk).

## Feedback

We welcome feedback on our app and documentation. Please email support@doorstepapp.com with your thoughts.

## Getting help

We do not provide any sales advice or after-sales advice or service. Any support we give is gratuitous and provided entirely at our discretion. Due to other work commitments, we cannot provide support face- to-face or via telephone under any circumstances - our sole communication method is via email (help@doorstepapp.com) or via our Website.
Please note that any support we provide relates to the configuration and use of Doorstep. We do not provide support for the configuration of web servers or similar systems.
Our support may be limited to the most recent version of Doorstep. We may, at our discretion, cease to provide support for earlier versions of Doorstep at any time without notice. You may have to update to the latest version available under your Licence and, failing to have done so already, we may ask you to complete the update before providing support.

Although we endevour to respond to support requests within a reasonable time, our ability to do so may be hindered by our capacity to take on new tasks at any given time. At present we do not offer support contracts or priority packages, nor do we currently have any plans to do so.

## Security Vulnerabilities

If you discover a security vulnerability within Doorstep, please send an e-mail to Papertank at hello@papertank.co.uk. All security vulnerabilities will be promptly addressed.

### License

For full details on licensing, please see your **license agreement** and [LICENCE.md](LICENSE.md).