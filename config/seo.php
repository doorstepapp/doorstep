<?php
return array(

    'title' => [
        'default' => 'Demo Doorstep Website',
        'suffix' => 'Demo',
        'glue' => ' - ',
        'separator' => ' | ',
    ],

    'description' => [
        'default' => '',
        'limit' => 156,
    ],

);